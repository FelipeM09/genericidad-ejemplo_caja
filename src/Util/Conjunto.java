/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 * Clase contenedora: Cada uno de sus elementos con cajas parametrizadas
 * Conjunto es una estructura da datos estática
 * @author MADARME
 */
public class Conjunto<T extends Comparable> {
    //Estructura de datos estática
    private Caja<T> []cajas;
    private int i=0;
    
    public Conjunto(){}
    
    public Conjunto(int cantidadCajas)
    {
    if(cantidadCajas <=0)
           throw new RuntimeException("No se pueden crear el Conjunto");
        
     this.cajas=new Caja[cantidadCajas];
    }
    
    
    public void adicionarElemento(T nuevo) throws Exception
    {
        if(i>=this.cajas.length)
            throw new Exception("No hay espacio en el Conjunto");
        
        if(this.existeElemento(nuevo))
            throw new Exception("No se puede realizar inserción, elemento repetido");
        
        
        this.cajas[i]=new Caja(nuevo);
        this.i++;
    
    }
    
    public T get(int indice)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        return this.cajas[indice].getObjeto();
            
    }
    
    
    public int indexOf(T objBuscar)
    {
    
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(objBuscar))
                return j;
        }
        
        return -1;
        
    }
    
    public void set(int indice, T nuevo)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        this.cajas[indice].setObjeto(nuevo);
            
    }
    
    
    public boolean existeElemento(T nuevo)
    {
        
        //Sólo estoy comparando por los estudiantes matriculados
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(nuevo))
                return true;
        }
        
        return false;
    
    }
    
    
    /**
     *  para el grupo A--> Selección
     *  para el grupo C--> Inserción
     * 
     */
    public void ordenar()
    {
      for(int i=0;i<cajas.length-1;i++){
          int min = i;
          for (int j=i+1;j<cajas.length;j++) {
              if(cajas[j].getObjeto().compareTo(cajas[min].getObjeto())<0){
                  min = j;
              }
          }
          Caja<T> aux = cajas[i];
          cajas[i]= cajas[min];
          cajas[min]=aux;
      }
    
    }
    
    
    /**
     * Realiza el ordenamiento por burbuja 
     */
    public void ordenarBurbuja()
    {
        Caja<T> aux;
        boolean ordenado = false;
        while(!ordenado){
            ordenado=true;
            for(int i =0;i<cajas.length-1;i++){
                if(cajas[i+1].getObjeto().compareTo(cajas[i].getObjeto())<0){
                    
                    aux = cajas[i];
                    cajas[i]= cajas[i+1];
                    cajas[i+1] = aux;
                    ordenado=false;
                    
                    
                }
            }
        }
    }
    
    /**
     * Elimina un elemento del conjunto y compacta
     * Ejemplo: Maria-->Manuel-->Karen-->Alberto  Eliminar a Karen es igual a
     * Maria-->Manuel-->Alberto-->     Queda el espacio al final.
     * 
     * @param objBorrado es el objeto que deseo eliminar
     */
    
    public void remover(T objBorrado) throws Exception
    {
        if(!this.existeElemento(objBorrado))
            throw new Exception("El objeto no existe");

        Caja<T> aux[] = new Caja[cajas.length-1];
        boolean entrar=true;

        for(int i=0; i<cajas.length-1 && entrar; i++){
            if(objBorrado.equals(cajas[i].getObjeto())){
                aux[i]=cajas[i+1];

                for(int f = i+1; f<cajas.length-1;f++){
                    aux[f]=cajas[f+1];
                    entrar=false;
                }
            }else
                aux[i]=cajas[i];
        }
        this.i=aux.length;
        this.setCajas(aux);
    }
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso no se toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,1,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenar(Conjunto<T> nuevo)
    {
        int contador=0;        
        Caja <T> aux [] = new Caja[this.cajas.length+nuevo.cajas.length];
        for(int i =0;i<this.cajas.length;i++){
            aux[i]= this.cajas[i];
            contador++;
        }
            for(int j=0;j<nuevo.cajas.length;j++){
                aux[contador]= nuevo.cajas[j];
                contador++;
        }
            contador=0;
            nuevo.removeAll();
            cajas = aux;
    }
    
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso SI toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenarRestrictivo(Conjunto<T> nuevo)
    {
        int contador=0, index=0;
        boolean limite = false;
        
        while(!limite){
            for(int i=0; i<this.cajas.length; i++){
                if(this.cajas[i].getObjeto().equals(nuevo.cajas[contador].getObjeto())){
                    nuevo.cajas[contador]=null;
                    index++;
                    contador++;
                    i=0;
                }
            }
            contador++;
            if(contador==nuevo.cajas.length){
                limite=true;
            }
        }
        
        Conjunto<T> temp=this.delete(nuevo, index);
        nuevo.removeAll();
        this.concatenar(temp);
    }
    
    
    public Conjunto<T> delete(Conjunto<T> n, int contador){
        
        Conjunto nuevo = new Conjunto((n.cajas.length)-contador);//3
        int index=0;
        
        for(int i=0; i<n.cajas.length; i++){
            if(n.cajas[i]!=null){
                nuevo.cajas[index]=n.cajas[i];
                index++;
            }
        }
        return nuevo;
    }
    
    public void removeAll()
    {
        this.cajas=null;
        this.i=0;
    }
    
    public Conjunto<T> getInterseccion(Conjunto <T> c2) throws Exception{
      int contador = 0;
      
        for(int f=0;f<this.cajas.length;f++){
            for(int h=0;h<c2.cajas.length;h++){
                if(this.cajas[f].getObjeto().equals(c2.cajas[h].getObjeto())){
                contador++;                
             }
           }            
        }
        Conjunto aux = new Conjunto(contador);        
        for(int j=0;j<this.cajas.length;j++){
            for(int h=0;h<c2.cajas.length;h++){
                if(this.cajas[j].getObjeto().equals(c2.cajas[h].getObjeto())){
                      aux.adicionarElemento(this.cajas[j].getObjeto());        
             }
           }               
        }
        return aux;
    }
    
    
    @Override
    public String toString() {
        String msg="******** CONJUNTO*********\n";
        
        for(Caja c:this.cajas)
            msg+=c.getObjeto().toString()+"\n";
        
        return msg;
    }
    
    
    
    /**
     * Obtiene la cantidad de elementos almacenados
     * @return  retorna un entero con la cantidad de elementos
     */
    public int getCapacidad()
    {
        return this.i;
    }
    
    /**
     *  Obtiene el tamaño máximo de cajas dentro del Conjunto
     * @return int con la cantidad de cajas
     */
    public int getLength()
    {
        return this.cajas.length;
    }
    
    /**
     * Obtiene el mayor elemento del Conjunto, recordar que el conjunto no posee elementos repetidos.
     * @return el elemnto mayor de la colección
     */
    public T getMayorElemento()
    {
    if(this.cajas==null)
        throw new RuntimeException("No se puede encontrar elemento mayor, el conjunto está vacío");
    
    T mayor=this.cajas[0].getObjeto();
    for(int i=1;i<this.getCapacidad();i++)
    {
        //Utilizo la interfaz comparable y después su método compareTo
        Comparable comparador=(Comparable)mayor;
        T dato_A_comparar=this.cajas[i].getObjeto();
        // Resta entra mayor-datoAComparar 
        int rta_compareTo=comparador.compareTo(dato_A_comparar);
        if(rta_compareTo<0)
            mayor=dato_A_comparar;
    }
    return mayor;
    }

    public Caja<T>[] getCajas() {
        return cajas;
    }

    public void setCajas(Caja<T>[] cajas) {
        this.cajas = cajas;
    }
    
}
