/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author MADARME
 */
public class TestNuevoConjunto {
    
      public static void main(String[] args) throws Exception {
        
               
        Conjunto<Integer> c1=new Conjunto(5);
        Conjunto<Integer> c2=new Conjunto(3);
        Conjunto<Integer> c3=new Conjunto(30);
        
        c1.adicionarElemento(100);
        c1.adicionarElemento(15);
        c1.adicionarElemento(20);
        c1.adicionarElemento(200);
        c1.adicionarElemento(1);
        
        c2.adicionarElemento(9);
        c2.adicionarElemento(1);
        c2.adicionarElemento(8);
        
        
        
        c1.ordenar();
        //c1.concatenar(c2);
        //c1.ordenarBurbuja();
        //c1.remover(2);
        
        mostrarConjunto(c1);
        
        
        
        
        //probar todos los métodos públicos(imprimir)
        //Recomendación: Realicen método para crear cada una de las pruebas.
        
        
    } 
    public static void mostrarConjunto(Conjunto<Integer> s) 
    {
       try{  
        System.out.println("__________________");
        System.out.println(s.toString());
       }catch(Exception e){
           System.out.println(e.getMessage());
       
       }
    }
}
